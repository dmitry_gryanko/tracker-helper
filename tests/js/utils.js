describe('Utils', function () {
	describe('trim()', function () {
		it('Without spaces', function () {
			expect(trim('string')).toBe('string');
		});

		it('With leading spaces', function () {
			expect(trim('  string')).toBe('string');
		});

		it('With ending spaces', function () {
			expect(trim('string   ')).toBe('string');
		});

		it('With both leading and ending spaces', function () {
			expect(trim('  string  ')).toBe('string');
		});

		it('With different whitespaces', function () {
			expect(trim(' \t\n\r string \t\n\r ')).toBe('string');
		});
	});

	describe('compareTitles()', function () {
		it('Equal titles', function () {
			expect(compareTitles('Task1', 'Task1')).toBe(true);
		});

		it('Different titles', function () {
			expect(compareTitles('Task1', 'Another Task2')).toBe(false);
		});

		it('Title with leading and ending spaces', function () {
			expect(compareTitles('Task1', ' Task1   ')).toBe(true);
		});

		it('Title with internal spaces', function () {
			expect(compareTitles('Task 1', 'Task  1')).toBe(true);
		});

		it('Title with several lines', function () {
			expect(compareTitles('Task 1', 'Task\n1')).toBe(true);
		});

		it('Title with tabs', function () {
			expect(compareTitles('Task 1', '\tTask \t1\t')).toBe(true);
		});

		it('With different letter case', function () {
			expect(compareTitles('Task 1', 'task 1')).toBe(true);
		});

		it('Complex text', function () {
			expect(compareTitles('24555\t- [Bug]Название бага\n\t\t ', '\n\t\t24555 - \t[Bug]Название   бага')).toBe(true);
		});

		it('A bit different strings', function () {
			expect(compareTitles('24555 - Task', '24555 _ Task')).toBe(false);
		});
	});
});
