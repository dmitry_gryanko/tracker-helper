var linkActive = $('span.TaskState>a:eq(0)');

var linkStandupList = $('<a/>')
	.clone()
	.text('StandUp')
	.addClass('ajax')
	.insertAfter(linkActive)
	.before('&nbsp;&nbsp;&nbsp;')
	.click(function() {
		// collecting information about available tasks
		var tasks = [];
		$('div.Task, div.Bug').each(function() {
			var task = {};
			task.id      = $('h3 span:eq(1)>a:eq(1)', this).attr('href').replace(/^.*?(\d+)\/$/, '$1');
			task.title   = $('h3 span:eq(1)>a:eq(1)', this).text().replace(/[→\s]{1,}/g, ' ').replace(/^\s+|\s+$/g, '');
			task.project = $('h3 span:eq(1)>a:eq(0)', this).text().replace(/\s{1,}/g, ' ').replace(/^\s+|\s+$/g, '');
			task.estimation = $('.TaskContent p.Small', this)
				.filter(function() {
					return $(this).text().match('Оценка');
				})
				.text()
				.replace(/\s{1,}/g, ' ')
				.replace(/^\s+|\s+$/g, '');
			tasks.push(task);
		});

		// extracting username if possible
		var m = /^[\s\S]*?Я\s+-\s+([а-яА-Я]+ [а-яА-Я]+)[\s\S]*$/.exec($('body>div:eq(0)>p:eq(0)').text());
		var username = m ? m[1] : null;

		// telling backgroud page to open new window
		chrome.extension.sendMessage({
			action: "standup_open",
			data: {
				tasks: tasks,
				username: username
			}
		});

		return false;
	});
