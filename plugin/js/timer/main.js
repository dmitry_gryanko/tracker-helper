/**
 * Forms date string for given date.
 * If date is not given then current date is used.
 *
 * @param date
 * @return string
 */
date = function(date) {
	if (!date) date = new Date();
	return sprintf('%02d.%02d.%04d', date.getDate(), date.getMonth() + 1, date.getFullYear());
};

$(function() {
	var settings = localStorage.timerWindow ? JSON.parse(localStorage.timerWindow) : {
		width : 400,
		height: 500,
		left  : 10,
		top   : 530
	};

	$(window).unload(function() {
		settings.left   = window.screenLeft;
		settings.top    = window.screenTop;
		settings.width  = window.outerWidth;
		settings.height = window.outerHeight;

		localStorage.timerWindow = JSON.stringify(settings);
	})
});


