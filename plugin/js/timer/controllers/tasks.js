function TasksController($scope, $filter) {
	var bgPage = chrome.extension.getBackgroundPage();

	$scope.tasks        = {};
	$scope.archive      = {};
	$scope.time         = 0;
	$scope.activeTask   = null;

	var port = chrome.extension.connect({name: 'timer'});
	port.onMessage.addListener(function (msg) {
		for (var i in msg) {
			if (i != 'activeTask') {
				$scope[i] = msg[i];
			}
		}

		$scope.activeTask = msg.activeTask !== undefined ? msg.activeTask : $scope.activeTask;
		$scope.activeTask = $scope.activeTask ? $scope.tasks[$scope.activeTask.id] : null;

		$scope.$apply();
	});

	$scope.newTask = {
		title: '',
		keyup: function (event) {
			if (event.keyCode == 27) {
				$scope.newTask.title = '';
			}
			else if (event.keyCode == 13) {
				// creating new task object with unique identity and selected name
				bgPage.timer.addTask($scope.newTask.title);

				// reset value of current edit box
				$scope.newTask.title = '';
			}
		}
	};

	$scope.toggleTask = function (task) {
		bgPage.timer.toggleTask(task);
	};

	$scope.hasTime = function (task) {
		return $filter('hours')(task.time) != '0.00';
	};

	$scope.noteTaskTime = function (task) {
		bgPage.timer.noteTaskTime(task.id);
	};

	$scope.removeTask = function(task) {
		bgPage.timer.removeTask(task.id);
	};

	$scope.restoreTask = function(task, dateStr) {
		bgPage.timer.restoreTask(task.id, dateStr);
	};

	$scope.gotoTracker = function(task, event) {
		bgPage.timer.gotoTracker(task.taskId);
		event.stopPropagation();
	};
}
