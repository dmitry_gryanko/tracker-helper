angular.module('Timer')
	// format seconds
	.filter('seconds', function () {
		return function (time) {
			return sprintf('%02d', Math.floor(time / 1000) % 60);
		};
	})

	// format minutes with hours
	.filter('minutes', function () {
		return function (time) {
			var min = Math.floor(time / 60000);
			return min < 60 ? min : sprintf('%d:%02d', Math.floor(min / 60), min % 60);
		};
	})

	// format hours
	.filter('hours', function () {
		return function (time) {
			return sprintf('%0.2f', time / 3600000);
		};
	})
;
