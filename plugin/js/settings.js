$(function() {
	// getting background page
	var bgPage = chrome.extension.getBackgroundPage();

	// applying stored settings
	var settings = bgPage.getSettings();
	for (var name in settings) {
		var value = settings[name];
		var el = $('[name='+name+']');

		if (el.attr('type') == 'checkbox') {
			if (value) {
				el.attr('checked', 'checked');
			}
			else {
				el.removeAttr('checked');
			}
		}
		else {
			el.val(value);
		}
	}

	// adding event for autosaving settings
	$('select').change(function() {
		var el = $(this);
		settings[el.attr('name')] = el.val();
		bgPage.setSettings(settings);
	});

	// adding event for autosaving settings
	$('input[type=checkbox]').change(function() {
		var el = $(this);
		settings[el.attr('name')] = el.attr('checked') ? el.val() : null;
		bgPage.setSettings(settings);
	});

	// open timer link
	$('#open_timer').click(function () {
		bgPage.timer.open();
		return false;
	});
});
