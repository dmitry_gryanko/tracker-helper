/**
 * Trims given string
 *
 * @param string
 * @returns {@String}
 */
function trim(string) {
	return string.replace(/^\s*([\s\S]*?)\s*$/, '$1');
}

/**
 * Compares two titles on similarity
 *
 * @param title1
 * @param title2
 * @return {@Boolean}
 */
function compareTitles(title1, title2) {
	var prepareValue = function (value) {
		return trim(value).toLowerCase().replace(/\s+/g, ' ');
	};

	return prepareValue(title1) == prepareValue(title2);
}
