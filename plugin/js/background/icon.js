var icon = {
	active: false
};

(function () {
	var canvas = document.createElement('canvas');
	canvas.width  = 19;
	canvas.height = 19;

	var context = canvas.getContext('2d');

	icon.redraw = function () {
		if (loading) return;

		context.clearRect(0, 0, canvas.width, canvas.height);

		context.drawImage(imageObj, 0, 0);

		context.beginPath();
		context.arc(16, 3, 3, 0, 2 * Math.PI);
		context.fillStyle = icon.active ? 'rgba(0, 230, 0, 1)' : 'rgba(240, 0, 0, 1)';
		context.fill();

		chrome.browserAction.setIcon({imageData: context.getImageData(0, 0, canvas.width,canvas.height)});
	};

	icon.setActive = function (status) {
		icon.active = status == undefined ? true : status;
		icon.redraw();
	};

	var loading = true;
	var imageObj = new Image();
	imageObj.onload = function() {
		loading = false;
		icon.redraw();
	};
	imageObj.src = 'img/favicon-19.png';
})();
