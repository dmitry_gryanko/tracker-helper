var getSettings, setSettings;

(function () {
	var ports = [];

	chrome.extension.onConnect.addListener(function(port) {
		if (port.name == "task-settings") {
			ports[port.sender.tab.id] = port;
			port.onDisconnect.addListener(function(port) {
				delete ports[port.sender.tab.id];
			});
			port.postMessage({settings: getSettings()});
		}
	});

	function sendSettings(settings) {
		for (var i in ports) {
			ports[i].postMessage({settings: settings});
		}
	}

	getSettings = function () {
		var settings = localStorage.getItem('settings') || '{}';
		settings = JSON.parse(settings);
		return settings;
	};

	setSettings = function (settings) {
		localStorage.setItem('settings', JSON.stringify(settings));
		sendSettings(settings);
	};
})();