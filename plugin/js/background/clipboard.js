clipboard = {};

(function($) {
	var tmp = $('<div/>').attr('id', 'tmp').appendTo('body');

	clipboard.copy = function (value) {
		tmp.text(value);

		var range = document.createRange();
		range.selectNodeContents(tmp[0]);
		var selection = window.getSelection();
		selection.removeAllRanges();
		selection.addRange(range);

		document.execCommand('copy');
	};

	chrome.extension.onMessage.addListener(
		function(request) {
			switch (request.action) {
				case 'copy2clipboard':
					clipboard.copy(request.data);
					break;
			}
		}
	);
})(jQuery);
