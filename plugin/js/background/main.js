var standupTabId = null;
var standupData = null;

chrome.extension.onMessage.addListener(
	function(request, sender, sendResponse) {
		// on standup-window request
		if (request.action == "standup_open") {
			standupData = request.data;
			chrome.tabs.create({
				url: 'html/standup.html'
			}, function (tab) {
				standupTabId = tab.id;
			});
			return;
		}

		if (request.action == "standup_ready") {
			// telling backgroud page to open new window
			chrome.tabs.sendMessage(standupTabId, {action: "standup_data", data: standupData});
			return;
		}

		var response = {};

		if (request.getSettings) {
			response['getSettings'] = getSettings();
		}

		sendResponse(response);
	}
);
