var timer = {
	windowId: null
};

(function () {
	var ports = [];

	var tasks   = localStorage.tasks   ? JSON.parse(localStorage.tasks)   : {};
	var archive = localStorage.archive ? JSON.parse(localStorage.archive) : {};

	var activeTask    = null;
	var time          = 0;
	var lastTimestamp = 0;

	if (!localStorage.next_id) {
		localStorage.next_id = 1;
	}

	chrome.extension.onConnect.addListener(function(port) {
		if (port.name == 'timer') {
			ports[port.sender.tab.id] = port;
			port.onDisconnect.addListener(function(port) {
				delete ports[port.sender.tab.id];
			});
			port.postMessage({
				tasks      : tasks,
				archive    : archive,
				activeTask : activeTask,
				time       : time
			});
		}
	});

	var sendData = function (data) {
		for (var i in ports) {
			ports[i].postMessage(data);
		}
	};

	var saveTasks = function () {
		localStorage.tasks = JSON.stringify(tasks);
		sendData({tasks: tasks});
	};

	var saveArchive = function () {
		localStorage.archive = JSON.stringify(archive);
		sendData({archive: archive});
	};

	var updateTime = function () {
		// getting current time
		var timestamp = new Date().getTime();
		// updating clock counter and the time of active task
		time             = time + timestamp - lastTimestamp;
		activeTask.time += timestamp - lastTimestamp;
		// saving last update time
		lastTimestamp = timestamp;

		// send data to subscribers
		sendData({time: time});
		saveTasks();
	};

	var date = function(date) {
		if (!date) date = new Date();
		return sprintf('%02d.%02d.%04d', date.getDate(), date.getMonth() + 1, date.getFullYear());
	};

	var findTask = function (title, taskId) {
		for (var id in tasks) {
			var task = tasks[id];
			if (compareTitles(title, task.name) && (!taskId || taskId == task.taskId)) {
				return task;
			}
		}
		return null;
	};

	var findTaskAndRestore = function (title, taskId) {
		for (var date in archive) {
			var tasks = archive[date];
			for (var id in tasks) {
				var task = tasks[id];
				if (compareTitles(title, task.name) && (!taskId || taskId == task.taskId)) {
					timer.restoreTask(task.id, date);
					return task;
				}
			}
		}
		return null;
	};

	timer.addTask = function (title, taskId) {
		var task = findTask(title, taskId) || findTaskAndRestore(title, taskId);
		if (!task) {
			task = {
				id        : localStorage.next_id++,
				name      : title,
				time      : 0,
				noted_time: 0,
				taskId    : taskId
			};

			// adding created task into the list, saving the list and showing the task
			tasks[task.id] = task;
			saveTasks();
		}

		// activating timer if necessary
		var settings = getSettings();
		if ((!activeTask || activeTask.id != task.id) && settings.timer_task_auto_start) {
			timer.toggleTask(task.id);
		}
	};

	timer.toggleTask = function (taskId) {
		// if there is any task already active at the moment
		if (activeTask) {
			// saving current task time
			updateTime();
			// if current active task is the same we're trying to toggle
			if (activeTask.id == taskId) {
				// activation of another is not necessary because we're just switching current task off
				activeTask = null;
				sendData({activeTask: activeTask, time: 0});
				icon.setActive(false);
				return;
			}
		}

		// remembering timestamp of current moment as the start of the current timer
		lastTimestamp = new Date().getTime();
		time          = 0;
		// saving link on current active task
		activeTask = tasks[taskId];
		sendData({time: time, activeTask: activeTask});
		icon.setActive(true);
	};

	timer.noteTaskTime = function (taskId, time) {
		task = tasks[taskId];
		time = time || task.time;
		task.noted_time += time;
		task.time -= time;

		saveTasks();
	};

	timer.removeTask = function(taskId) {
		// always noting task time before removal
		timer.noteTaskTime(taskId);

		// switching of the timer if the task being removed is active
		if (activeTask && activeTask.id == taskId) {
			timer.toggleTask(taskId);
		}

		// form current date string and initialize sub-list in the archive
		var dateStr = date();
		if (!archive[dateStr]) {
			archive[dateStr] = {};
		}

		// copy removed task into archive
		archive[dateStr][taskId] = tasks[taskId];

		// removing task from the list and saving it
		delete tasks[taskId];

		// saving
		saveArchive();
		saveTasks();
	};

	timer.restoreTask = function(taskId, dateStr) {
		// copy restored task into the main list
		tasks[taskId] = archive[dateStr][taskId];

		// removing task from the archive
		delete archive[dateStr][taskId];

		// removing whole date if it became empty
		if (Object.keys(archive[dateStr]).length == 0) {
			delete archive[dateStr];
		}

		// saving
		saveTasks();
		saveArchive();
	};

	timer.gotoTracker = function (trackerId) {
		chrome.windows.getAll({populate: true}, function (windows) {
			var taskWindow, taskTab;
			var regex = new RegExp('^http://tracker.wapdev.ru/task/'+ trackerId +'/');

			for (var i in windows) {
				var window = windows[i];
				for (var j in window.tabs) {
					var tab = window.tabs[j];
					if (tab.url.match(regex)) {
						taskTab = tab;
						taskWindow = window;
						break;
					}
				}
			}

			if (taskTab) {
				chrome.tabs.update(taskTab.id, {active: true});
			}
			else {
				taskWindow = windows[0];
				chrome.tabs.create({
					windowId: taskWindow.id,
					url: 'http://tracker.wapdev.ru/task/'+ trackerId +'/',
					active: true
				});
			}
			chrome.windows.update(taskWindow.id, {focused: true});
		});
	};

	// main clock timer (fires once a second)
	setInterval(function() {
		// do not do anything if there is no active task
		if (!activeTask) {
			return;
		}

		// update task's time and refresh timer counters
		updateTime();
	}, 1000);

	// open timer link
	timer.open = function () {
		if (timer.windowId) {
			chrome.windows.update(timer.windowId, {focused: true});
			return;
		}

		var settings = localStorage.timerWindow ? JSON.parse(localStorage.timerWindow) : {
			width : 400,
			height: 500,
			left  : 10,
			top   : 530
		};

		chrome.windows.create({
			url    : "html/timer.html",
			focused: true,
			width  : settings.width,
			height : settings.height,
			left   : settings.left,
			top    : settings.top,
			type   : 'popup'
		}, function (window) {
			timer.windowId = window.id;
		});
	};

	chrome.windows.onRemoved.addListener(function (windowId) {
		if (windowId == timer.windowId) {
			timer.windowId = null;
		}
	});

	var settings = getSettings();
	if (settings['timer_auto_open']) {
		timer.open();
	}

	chrome.extension.onMessage.addListener(
		function(request) {
			switch (request.action) {
				case 'timer_addTask':
					timer.addTask(request.data.title, request.data.id);
					break;

				case 'timer_toggleTask':
					timer.toggleTask(request.data.id);
					break;

				case 'timer_removeTask':
					timer.removeTask(request.data.id);
					break;

				case 'timer_noteTaskTime':
					timer.noteTaskTime(request.data.id, request.data.time);
					break;
			}
		}
	);
})();
