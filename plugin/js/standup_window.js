/**
 * Updates status of redundant tasks
 */
function updateRedundantTasks()
{
	$('.tasks>.task:lt(14)').removeClass('redundant');
	$('.tasks>.task:gt(13)').addClass('redundant');
}

$(function() {
	// put current date
	var date = new Date();
	$('span.date').text(date.toDateString());

	// add print button action
	$('button.print').click(function() {
		window.print();
	});

	// add trash-bin functionality
	$('button.trash').click(function() {
		console.log($(this).parents('div.task'));
		$(this).parents('div.task').remove();
		updateRedundantTasks();
	});

	// telling backgroud page that standup window is ready for action
	chrome.extension.sendMessage({
		action: 'standup_ready'
	});
});

chrome.extension.onMessage.addListener(
	function(request, sender, sendResponse) {
		// ignore all messages except data itself
		if (request.action != 'standup_data') {
			return;
		}

		var divTasks      = $('div.tasks.filled');
		var divTasksEmpty = $('div.tasks.empty');

		if (request.data.username) {
			$('.username', el).text(request.data.username);
		}

		for (var i in request.data.tasks) {
			var task = request.data.tasks[i];

			var el = $('.task.filled.template').clone(true);
			el
				.removeClass('template')
				.appendTo(divTasks)
			;
			$('.id', el).text(task.id);
			$('.title', el).text(task.title);
			$('.project', el).text(task.project);
			$('.estimation', el).text(task.estimation);
		}

		// adding some empty tasks
		for (i = 0; i < 6; i++) {
			console.log(i);
			$('.task.empty.template')
				.clone()
				.removeClass('template')
				.appendTo(divTasksEmpty)
			;
		}

		updateRedundantTasks();

		$(".tasks.filled")
			.sortable({
				stop: function() {
					updateRedundantTasks();
				}
			})
			.disableSelection();
	}
);
