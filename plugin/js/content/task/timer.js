(function () {
	var h3 = $('.Task h3');
	var aTitle = h3.find('a:eq(1)');

	var task = {
		active: false,
		id    : undefined,
		taskId: aTitle.attr('href').replace(/^.*?(\d+)(\/\w+)*\/$/, '$1')
	};

	var div = $('<div/>')
		.addClass('timer-task')
		.appendTo('body')
		.click(function () {
			if (task.id) {
				chrome.extension.sendMessage({
					action: 'timer_toggleTask',
					data: {
						id: task.id
					}
				});
			}
			else {
				chrome.extension.sendMessage({
					action: 'timer_addTask',
					data: {
						id   : task.taskId,
						title: aTitle.text().replace(/^[\s→]*(.*)\s*$/, '$1')
					}
				});
			}

			return false;
		});

	var time = $('<div/>')
		.text('')
		.appendTo(div);

	var port = chrome.extension.connect({name: 'timer'});
	port.onMessage.addListener(function (msg) {
		if (msg.tasks) {
			if (task.id) {
				if (!msg.tasks[task.id]) {
					task.id = null;
				}
			}
			else {
				task.id = null;
				for (var i in msg.tasks) {
					if (msg.tasks[i].taskId == task.taskId) {
						task.id = msg.tasks[i].id;
						break;
					}
				}
			}
		}

		var input  = $('#TimebarForm input[name=timebar_spend]');
		var inputs = $('#TimebarForm').find('input[name=timebar_spend], input[name=timebar_left]');

		if (msg.activeTask !== undefined) {
			task.active = msg.activeTask && msg.activeTask.taskId == task.taskId;
			if (task.active) {
				div.addClass('active');
				inputs
					.attr('readonly', 'on')
					.attr('title', 'Для ручного редактирования требуется остановить таймер');
			}
			else {
				div.removeClass('active');
				inputs
					.removeAttr('readonly')
					.removeAttr('title');
			}
		}

		if (msg.tasks) {
			task.time = task.id ? msg.tasks[task.id].time : 0;
			var hours = task.time / 3600000;
			time.text(sprintf('%0.2f h', hours));

			if (task.active) {
				input.val(sprintf('%0.1f', hours + 0.07));
				calculateTimebar();
			}

			if (hours >= 0.005) {
				div.addClass('visible');
			}
			else {
				div.removeClass('visible');
			}
		}
	});

	$(document).on('click', '#TimebarForm form input[type=submit]', function () {
		if ($('#timebarComment').val().length == 0) {
			return;
		}

		if (!task.id) {
			return;
		}

		var noted = getTimeValue($('#timebarSpend').val()) * 3600000;
		var left = task.time - noted;
		if (left <= 0) {
			chrome.extension.sendMessage({
				action: 'timer_removeTask',
				data: {
					id: task.id
				}
			});
		}
		else if (noted > 0) {
			chrome.extension.sendMessage({
				action: 'timer_noteTaskTime',
				data: {
					id  : task.id,
					time: noted
				}
			});
		}
	});

	// functions from tracker intact
	function getTimeValue(timeField) {
		var getTime = 0;
		if (timeField)
			getTime = parseFloat(timeField.replace(",", "."));

		return getTime;
	}
	function calculateTimebar() {
		var val = getTimeValue($('#timebarLeftS').val()) - getTimeValue($('#timebarSpend').val());
		var all = Math.abs(getTimeValue($('#timebarAllS').val()));
		$('#timebarAll').val(getTimeValue($('#timebarAllS').val()));
		if (val < 0) {
			$('#timebarLeft').val(0);
			$('#timebarAll').val((all + Math.abs(val)).toFixed(2) == Math.round(all + Math.abs(val)) ? all + Math.abs(val) : (all + Math.abs(val)).toFixed(2));
		}
		else {
			$('#timebarLeft').val(val.toFixed(2) == Math.round(val) ? val : val.toFixed(2));
		}
	}
})();
