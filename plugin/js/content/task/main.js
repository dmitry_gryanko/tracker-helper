// marking all comments of changed deadline
$('.TaskContent.Comment').each(function () {
	if ($(this).text().match(/^\s*Дедлайн задачи изменен:[\s\S]*По причине:/)) {
		$(this).parents('div:eq(2)').addClass('deadline-changed');
	}
});

/**
 * Adjusts visibility of deadline-changed comments
 *
 * @param type
 */
var hideDeadlineComments = function(type)
{
	if (!type || type == 'none') {
		$('.deadline-changed').show();
	}
	else {
		$('.deadline-changed').hide();
		if (type == 'except-last') {
			$('.deadline-changed:last').show();
		}
	}
};

// subscribe on settings changes
var port = chrome.extension.connect({name: "task-settings"});
port.onMessage.addListener(function(msg) {
	hideDeadlineComments(msg.settings.hide_comments_deadline_changed);
});


$('.TaskContent.Comment p br')
	.replaceWith(' ');

$('#TimebarForm').find('textarea')
	.parent().css('padding', '0 6px 0 3px');


$('#commentForm textarea, #TimebarForm textarea').keyup(function () {
	var linesQty = $(this).val().split('\n').length;
	$(this).css('height', ((Math.min(45, linesQty) + 4) * 1.2) + 'em');
});
