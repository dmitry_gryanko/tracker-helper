$('.TaskDescription, .TaskContent.Comment').each(function() {
	$(this).html($(this).html().replace(/((feature|fix)\/\w+\/\d+)/, '<span class="th-branch" title="copy to clipboard">$1</span>'));
});

$(document).on('click', 'span.th-branch', function () {
	chrome.extension.sendMessage({
		action: 'copy2clipboard',
		data: $(this).text()
	});
});
