// subscribe on settings changes
var port = chrome.extension.connect({name: "task-settings"});
port.onMessage.addListener(function(msg) {
	var panels = $('.Panel.Small');
	if (msg.settings.hide_small_panels) {
		panels.hide();
	}
	else {
		panels.show();
	}
});
