// subscribe on settings changes
var port = chrome.extension.connect({name: "task-settings"});
port.onMessage.addListener(function(msg) {
	var panel = $('.ActivityPanel');
	if (msg.settings.hide_last_comments) {
		panel.hide();
	}
	else {
		panel.show();
	}
});
