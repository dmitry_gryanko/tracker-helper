$(function(){
	$('div:first').addClass('print-hidden');
	$('h1:first').addClass('print-hidden');
	$('.Panel').addClass('print-hidden');
	$('.Menu1').addClass('print-hidden');
	$('#paddingdiv').find('> p').addClass('print-hidden');

	var f = false;
	$('table:first tr').each(function(){
		if (!$(this).hasClass('timebar0')) {
			f = true;
		}
		if (f) {
			$(this).addClass('print-hidden');
		}
	});

	$('*[title="Потрачено"]').each(function(){
		if ($(this).text() == '+0') {
			$(this).parents('tr:first').addClass('print-hidden');
		}
		else {
			$(this).parents('div:first')
				.addClass('print-hidden')
				.after(
					$('<div/>')
						.text($(this).text().replace('\+', ''))
						.addClass('screen-hidden')
				)
			;
		}
	});

	// удаляем бээрку
	$('#paddingdiv').find('> table').prev().addClass('print-hidden');

	// удаляем лишние времена
	$('*[title="Осталось"]').addClass('print-hidden');
	$('*[title="Оценка"]').addClass('print-hidden');
	$('*[title="Потрачено всего"]').addClass('print-hidden');

	// удаляем дату
	$('.timebar0 p b').parent().addClass('print-hidden');

	// удаляем taskstate
	$('.TaskState').addClass('print-hidden')
		.next('a').addClass('print-hidden');
});