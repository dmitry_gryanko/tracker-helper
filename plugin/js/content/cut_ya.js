function hideYa(element) {
	// checking existence of "Я -"
	var m = /^\s*(<nobr>)?(Я\s*[—-]\s*)([\s\S]+(?:<\/nobr>)?)$/i.exec(element.html());
	if (!m) {
		return;
	}

	// wrapping "Я -" into span
	element.html('<span class="th-ya">' + m[2] + '</span>' + m[3]);
}

$('body>div:first>p:first').each(function() {
	hideYa($(this));
});
$('a, .MenuSelected').each(function() {
	hideYa($(this));
});

// subscribe on settings changes
var port = chrome.extension.connect({name: "task-settings"});
port.onMessage.addListener(function(msg) {
	var spans = $('span.th-ya');
	if (msg.settings.hide_ya) {
		spans.hide();
	}
	else {
		spans.show();
	}
});
